(ns gutter.monad
  (:require [clojure.algo.monads :refer [with-monad defmonad domonad]]))

(defmonad error-m
  [m-result (fn [v] [v nil])
   m-bind (fn [[v e] f] (if e [nil e] (f v)))])

(defmacro doerror
  [bindings ret]
  `(domonad error-m ~bindings ~ret))

(defmacro with-error
  [& body]
  `(with-monad error-m ~@body))
