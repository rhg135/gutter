(ns gutter.util
  (:import org.jsoup.Jsoup
           [org.jsoup.nodes Document Element])
  (:require [org.httpkit.client :as http]
            [clojure.string :as string]))

; HTTP

(def ^:dynamic *user-agent* "gutter 0.0.1")

(defn GET
  [url]
  (let [get-args [url {:user-agent *user-agent*}]
        {:keys [body error]} @(apply http/get get-args)]
    (if error
      [nil {:message "GET: network issue?" :error error :call `(GET ~@get-args)}]
      [body nil])))

; Jsoup

(defn parse
  [^String body ^String base]
  (try [(if base (Jsoup/parse body base) (Jsoup/parse body)) nil]
       (catch Exception e
         [nil {:message "parse: parse failure" :exception e :call `(parse ~body ~base)}])))

(defn select
  [^Document doc sel]
  (try [(let [tags (-> (.select doc sel) seq)]
         (for [tag tags] ^Element tag)) nil]
       (catch Exception e
         [nil {:message "select: select failed"
               :exception e :call `(select ~doc ~sel)}])))

(defn html
  [^Element el]
  (.html el))

(defn attributes
  [^Element el]
  (try
    (let [attrs (.attributes el)]
      [(into {} (for [attr attrs]
                  [(keyword (.getKey attr)) (.getValue attr)])) nil])
      (catch Exception e
        [nil {:message "couldn't get attributes"
              :exception e
              :call `(attributes ~el)}])))

(defn classes
  [^Element el]
  (try
    [(set (map keyword (.classNames el))) nil]
    (catch Exception e
      [nil {:messge "couldn't get classes"
            :exception e
            :call `(classes ~el)}])))
