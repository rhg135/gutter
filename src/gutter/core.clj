(ns gutter.core
  (:require [gutter.monad :refer [error-m]]
            [gutter.util :as gu]
            [clojure.algo.monads :as m]
            [clojure.core.async :refer [go chan close! put! <!!]]))

(defn m-return
  [v]
  (go v))

(defn m-bind
  [ch f]
  (<!! (go (f (<! ch)))))

(m/defmonad channel-m
  [m-result m-return
   m-bind m-bind])

(defn url->doc
  ([url base-url]
   (m-return
     (m/domonad error-m
                [body (gu/GET url)
                 doc (gu/parse url base-url)]
                doc))))
