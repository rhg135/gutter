# gutter

A Clojure library designed to be used to write scrapers

## Usage

Grabbing a Document:

	(require 'gutter.core)
	(gutter.core/url->doc "http://blah.com/123" "http://blah.com/")
	; => <ManyToManyChannel...>

Getting a seq of elements:

	(require 'gutter.util)
	(gutter.util/select some-doc "some selector")
	; => [(...) maybe-an-error]

## Contributing

I'm not afraid of the command line, I welcome raw patches if you don't want to use the web gui.
Post it on refheap (or your favorite paste site) and hunt me down.

## License

Copyright © 2014 Ricardo Gomez

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
